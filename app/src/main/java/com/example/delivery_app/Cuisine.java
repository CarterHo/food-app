package com.example.delivery_app;

public enum Cuisine {
    BURGERS("Burgers"),
    CHICKEN("Chicken"),
    FISH("Fish"),
    VEGGIES("Veggies"),
    SIDES("Sides"),
    BREAKFAST("Breakfast"),
    KING_JR("King Jr"),
    SWEETS("Sweets"),
    DRINKS("Drinks"),
    COFFEE("Coffee"),
    SHAKES("Shakes");

    Cuisine(String cuisineName) {
        this.cuisineName = cuisineName;
    }

    @Override
    public String toString() {
        return cuisineName;
    }

    // Private properties
    private String cuisineName;

}
