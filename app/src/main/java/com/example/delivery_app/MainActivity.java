package com.example.delivery_app;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private String imageResourceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Dish dish1 = new Dish(imageResourceName);
        System.out.println("Dish 1:" + dish1);
        Dish dish2 = new Dish(imageResourceName);
        System.out.println("Dish 2:" + dish2);
        Dish dish3 = new Dish(imageResourceName);
        System.out.println("Dish 3:" + dish3);

        System.out.println("dish1 == dish2" + dish1.equals(dish2));
        System.out.println("dish1 == dish3" + dish1.equals(dish3));
        System.out.println("dish2 == dish3" + dish2.equals(dish3));

        Menu menu = new Menu();
        HashMap<String, ArrayList<Dish>> dishesByCuisine = menu.dishesByCuisine();
    }
}