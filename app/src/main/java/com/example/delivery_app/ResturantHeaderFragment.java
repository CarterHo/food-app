 package com.example.delivery_app;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ResturantHeaderFragment extends Fragment {

    public ResturantHeaderFragment() {
        // Required empty public constructor
    }

    public static ResturantHeaderFragment newInstance() {
        ResturantHeaderFragment fragment = new ResturantHeaderFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setupViews();
        return inflater.inflate(R.layout.fragment_resturant_header, container, false);
    }

    //Private methods
    private void setupViews() {

    }
}